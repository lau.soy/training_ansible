# Vagrant

Pour provisionner vos VMs assurez vous d'avoir une paire de clé présente sur votre système dans votre dossier `~/.ssh/` (ca marche aussi sous windows)

Il faut ensuite installer [Vagrant](https://www.vagrantup.com/downloads) et [Virtualbox](https://www.virtualbox.org/wiki/Downloads). Il peut être necessaire de désactiver HyperV et docker sur le poste s'ils sont présents. Une fois que tout est installé, se positionner dans le dossier `vagrant` et passer la commande `vagrant up`.

Vagrant va provisionner:
- 1 VM appelée `master` qui servira à passer les commandes ansibles (192.168.50.10)
- 1 VM appelée `production` qui sera notre serveur de production (192.168.50.11)
- 1 VM appelée `recette` qui sera notre serveur de recette (192.168.50.12)
- Ces VMs disposeront d'un réseau privée avec l'hôte afin de pouvoir travailler avec des IP fixes

Configuration: 
- Les VMs sont en ubuntu 18.04, et seront provisionnées avec le strict minimum, ansible fera le reste.
- La clé perso SSH est copiée sur `master` pour pouvoir se connecter dessus depuis l'hôte.
- Les Vms `production`et `recette` seront accessibles depuis `master`en SSH sur le compte `ansible`. La clé publique de `master` est copié pendant l'installation.
- le compte `ansible` dispose des droits pour sudo
- sur `master` le fichier `/etc/hosts` sera à jour avec les entrées des VMs `production`et `recette`

Sur votre poste de travail:
Mettre à jour le ficher `hosts` en ajoutant les entrées suivantes:

```txt
192.168.50.11 app.local production
192.168.50.12 rec-app.local recette
```

