# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :
# Box / OS
VAGRANTFILE_API_VERSION = "2"

VAGRANT_BOX = 'ubuntu/bionic64'
VM_USER = 'vagrant'

nodes = {
  "production" => { :ip => "192.168.50.11" },
  "recette" => { :ip => "192.168.50.12" }
}

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.synced_folder '.', '/home/vagrant/shared', SharedFoldersEnableSymlinksCreate: true

  config.vm.define "master" do |master|
    # IP Statique
    master.vm.network "private_network", ip: "192.168.50.10"
    master.vm.box = VAGRANT_BOX

    config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "/home/vagrant/.ssh/me.pub"
    config.vm.provision "file", source: ".ssh/id_rsa", destination: "/home/vagrant/.ssh/id_rsa"

    # Actual machine name
    master.vm.hostname = "master"
    master.vm.provision "shell", inline: <<-SHELL
          
          # installs
          apt-get update -y
          add-apt-repository --yes --update ppa:ansible/ansible
          apt-get install software-properties-common python3.8 git ansible ansible-lint -y
          apt-get upgrade -y
          apt-get autoremove -y

          # SSH settings
          chown vagrant /home/vagrant/.ssh/id_rsa
          chmod 400 /home/vagrant/.ssh/id_rsa
          cat /home/vagrant/.ssh/me.pub >> /home/vagrant/.ssh/authorized_keys

      SHELL

      # mise a jour de etc/hosts
      nodes.each_with_index do |(hostname, info), index|
        master.vm.provision "shell", inline: "echo \"#{info[:ip]} #{hostname}\" >> /etc/hosts"
      end

  end

  nodes.each_with_index do |(hostname, info), index|

    config.vm.define hostname do |node|

      node.vm.network "private_network", ip: "#{info[:ip]}"
      node.vm.box = VAGRANT_BOX

      config.vm.provision "file", source: ".ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/master.pub"

      # Actual machine name
      node.vm.hostname = hostname
      node.vm.provision "shell", inline: <<-SHELL

            # installs
            apt-get install software-properties-common python3.8 -y
            apt-get upgrade -y
            apt-get autoremove -y

            # ansible user
            useradd -m ansible
            mkdir /home/ansible/.ssh
            cat /home/vagrant/.ssh/master.pub >> /home/ansible/.ssh/authorized_keys
            chown -R ansible /home/ansible/.ssh

            #donne les droits sudo a ansible
            echo "ansible ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
        SHELL
    end
  end

end
